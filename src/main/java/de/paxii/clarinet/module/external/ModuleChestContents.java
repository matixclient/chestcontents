package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.ReceivePacketEvent;
import de.paxii.clarinet.event.events.game.RenderTickEvent;
import de.paxii.clarinet.event.events.player.PreMotionUpdateEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderEntityItem;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.projectile.EntitySnowball;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.CPacketAnimation;
import net.minecraft.network.play.client.CPacketCloseWindow;
import net.minecraft.network.play.client.CPacketPlayer;
import net.minecraft.network.play.client.CPacketPlayerTryUseItemOnBlock;
import net.minecraft.network.play.server.SPacketOpenWindow;
import net.minecraft.network.play.server.SPacketWindowItems;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;

import org.lwjgl.opengl.GL11;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author Wdestroier
 */
public class ModuleChestContents extends Module {

  private HashMap<TileEntityChest, ArrayList<ItemStack>> loadedChests = new HashMap<>();
  private TileEntityChest nextChest;
  private SPacketOpenWindow chestPacket;

  public ModuleChestContents() {
    super("ChestContents", ModuleCategory.RENDER);

    this.setVersion("1.0");
    this.setBuildVersion(18809);
    this.setDescription("Displays contents of chests once you get near them.");
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this, RenderTickEvent.class);
    Wrapper.getEventManager().register(this, PreMotionUpdateEvent.class);
  }

  @EventHandler
  public void onPreMotionUpdate(PreMotionUpdateEvent event) {
    if (this.nextChest == null) {
      Optional<TileEntityChest> tileEntityChestOptional = Wrapper.getWorld().loadedTileEntityList.stream()
              .filter(TileEntityChest.class::isInstance)
              .map(TileEntityChest.class::cast)
              .filter(chest -> !this.loadedChests.containsKey(chest) && this.getDistance(chest.getPos()) <= 6.0D)
              .filter(chest -> !this.isAdjacentChest(chest))
              .findAny();

      if (!tileEntityChestOptional.isPresent()) {
        return;
      }

      this.nextChest = tileEntityChestOptional.get();
    }

    if (Wrapper.getMinecraft().currentScreen != null) {
      nextChest = null;
      return;
    }

    BlockPos pos = nextChest.getPos();
    float[] angles = getAngles(pos);

    Wrapper.getSendQueue().addToSendQueue(new CPacketPlayer.Rotation(angles[0], angles[1], Wrapper.getPlayer().onGround));
    Wrapper.getSendQueue().addToSendQueue(new CPacketAnimation(EnumHand.MAIN_HAND));
    Wrapper.getSendQueue().addToSendQueue(new CPacketPlayerTryUseItemOnBlock(pos, getFacing(pos), EnumHand.MAIN_HAND, 0.0F, 0.0F, 0.0F));
    Wrapper.getEventManager().unregister(this, PreMotionUpdateEvent.class);
    Wrapper.getEventManager().register(this, ReceivePacketEvent.class);
  }

  @EventHandler
  public void onReceivePacket(ReceivePacketEvent receivePacketEvent) {
    if (receivePacketEvent.getPacket() instanceof SPacketOpenWindow) {
      SPacketOpenWindow packet = (SPacketOpenWindow) receivePacketEvent.getPacket();
      if ("minecraft:chest".equals(packet.getGuiId())) {
        chestPacket = packet;
        receivePacketEvent.setCancelled(true);
      } else {
        Wrapper.getEventManager().unregister(this, ReceivePacketEvent.class);
        nextChest = null;
        Wrapper.getEventManager().register(this, PreMotionUpdateEvent.class);
      }
    }

    if (receivePacketEvent.getPacket() instanceof SPacketWindowItems) {
      SPacketWindowItems packet = (SPacketWindowItems) receivePacketEvent.getPacket();
      if (packet.getWindowId() == chestPacket.getWindowId()) {
        ContainerChest container = new ContainerChest(Wrapper.getPlayer().inventory, new InventoryBasic(chestPacket.getWindowTitle(), chestPacket.getSlotCount()), Wrapper.getPlayer());
        container.setAll(packet.getItemStacks());
        ArrayList<ItemStack> chestItems = new ArrayList<>();
        for (int i = 0; i < container.getLowerChestInventory().getSizeInventory(); i++) {
          ItemStack stack = container.getLowerChestInventory().getStackInSlot(i);
          chestItems.add(stack);
        }
        Collections.reverse(chestItems);
        //TODO check for duplicates (when the player goes outside the chunk and comes back, the world creates a new tile entity, maybe check world#unloadedTileEntityList) or onRender { if not in world then remove }
        loadedChests.put(nextChest, chestItems);
        Wrapper.getEventManager().unregister(this, ReceivePacketEvent.class);
        Wrapper.getSendQueue().addToSendQueue(new CPacketCloseWindow(packet.getWindowId()));
        nextChest = null;
        Wrapper.getEventManager().register(this, PreMotionUpdateEvent.class);
        receivePacketEvent.setCancelled(true);
      }
    }
  }

  @EventHandler
  public void onGlobalRender(RenderTickEvent renderTickEvent) {
    loadedChests.forEach((chest, items) -> {
      this.draw(chest, items, renderTickEvent.getRenderPartialTicks());
    });
  }

  private void draw(TileEntityChest chest, ArrayList<ItemStack> itemList, float partialTicks) {
    FontRenderer fontRenderer = Wrapper.getMinecraft().getRenderManager().getFontRenderer();
    RenderEntityItem renderEntity = (RenderEntityItem) Wrapper.getMinecraft().getRenderManager().getEntityRenderMap().get(EntityItem.class);
    BlockPos blockPos = chest.getPos().up(chest.getSizeInventory() == 27 ? 1 : 3);
    EntityPlayerSP entityPlayer = Wrapper.getPlayer();
    double posX = blockPos.getX() - (entityPlayer.lastTickPosX + (entityPlayer.posX - entityPlayer.lastTickPosX) * partialTicks);
    double posY = blockPos.getY() - (entityPlayer.lastTickPosY + (entityPlayer.posY - entityPlayer.lastTickPosY) * partialTicks);
    double posZ = blockPos.getZ() - (entityPlayer.lastTickPosZ + (entityPlayer.posZ - entityPlayer.lastTickPosZ) * partialTicks);

    GL11.glPushMatrix();
    GlStateManager.translate(posX, posY, posZ);
    GlStateManager.rotate(-Wrapper.getMinecraft().getRenderManager().playerViewY, 0.0F, 1.0F, 0.0F);
    GlStateManager.disableLighting();
    GlStateManager.depthMask(false);
    GlStateManager.disableDepth();
    GlStateManager.enableBlend();
    GlStateManager.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);

    int xIndex = 0;
    int yIndex = 0;
    for (ItemStack itemStack : itemList) {
      if (itemStack == null) {
        continue;
      }

      EntityItem entityItem = new EntityItem(Wrapper.getWorld(), posX, posY, posZ, new ItemStack(itemStack.getItem(), 1, itemStack.getMetadata()));
      entityItem.hoverStart = 0;
      GlStateManager.scale(-0.01F, -0.01F, 0.01F);

      if (Item.getIdFromItem(itemStack.getItem()) != 0) {
        GlStateManager.scale(2.0D, 2.0D, 2.0D);
        fontRenderer.drawString(itemStack.getStackSize() + "x", (190 - (xIndex * 50)) / 2, yIndex * -27, 0xFFFFFFFF);
        GlStateManager.scale(0.5D, 0.5D, 0.5D);
      }

      GlStateManager.scale(-100F, -100F, 100F);
      renderEntity.doRender(entityItem, -2D + (xIndex * 0.5D), (yIndex * 0.5D), 0, entityPlayer.rotationYaw, partialTicks);
      RenderHelper.disableStandardItemLighting();

      xIndex++;
      if (itemList.indexOf(itemStack) > 0 && (itemList.indexOf(itemStack) + 1) % 9 == 0) {
        xIndex = 0;
        yIndex++;
      }
    }

    GlStateManager.enableDepth();
    GlStateManager.depthMask(false);
    GlStateManager.enableLighting();
    GlStateManager.disableBlend();
    GL11.glPopMatrix();
  }

  private float[] getAngles(final BlockPos blockPos) {
    double difX = (blockPos.getX() + 0.5D) - Wrapper.getPlayer().posX,
            difY = (blockPos.getY() + 0.5D)
                    - (Wrapper.getPlayer().posY + Wrapper.getPlayer().getEyeHeight()),
            difZ = (blockPos.getZ() + 0.5D) - Wrapper.getPlayer().posZ;
    double helper = Math.sqrt(difX * difX + difZ * difZ);
    float yaw = (float) (Math.atan2(difZ, difX) * 180 / Math.PI) - 90;
    float pitch = (float) -(Math.atan2(difY, helper) * 180 / Math.PI);
    return (new float[]{yaw, pitch});
  }

  private EnumFacing getFacing(BlockPos pos) {
    final EnumFacing[] array = new EnumFacing[]{EnumFacing.UP, EnumFacing.NORTH, EnumFacing.EAST, EnumFacing.SOUTH, EnumFacing.WEST};

    for (final EnumFacing facing : array) {
      final Entity temp = new EntitySnowball(Wrapper.getWorld());
      temp.posX = pos.getX() + 0.5;
      temp.posY = pos.getY() + 0.5;
      temp.posZ = pos.getZ() + 0.5;
      temp.posX += facing.getDirectionVec().getX() * 0.5;
      temp.posY += facing.getDirectionVec().getY() * 0.5;
      temp.posZ += facing.getDirectionVec().getZ() * 0.5;

      if (Wrapper.getPlayer().canEntityBeSeen(temp)) {
        return facing;
      }
    }

    return EnumFacing.DOWN;
  }

  private double getDistance(BlockPos pos) {
    return Wrapper.getPlayer().getDistance(pos.getX(), pos.getY(), pos.getZ());
  }

  private boolean isAdjacentChest(TileEntityChest tileEntityChest) {
    if (!tileEntityChest.adjacentChestChecked) {
      tileEntityChest.checkForAdjacentChests();
    }
    Optional<TileEntityChest> adjacentChest = Stream.of(
            tileEntityChest.adjacentChestXPos,
            tileEntityChest.adjacentChestZPos,
            tileEntityChest.adjacentChestXNeg,
            tileEntityChest.adjacentChestZNeg
    ).filter(Objects::nonNull).findFirst();

    return adjacentChest.isPresent() && loadedChests.containsKey(adjacentChest.get());
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this, PreMotionUpdateEvent.class);
    Wrapper.getEventManager().unregister(this, ReceivePacketEvent.class);
    Wrapper.getEventManager().unregister(this, RenderTickEvent.class);

    loadedChests.clear();
    nextChest = null;
    chestPacket = null;
  }

}
